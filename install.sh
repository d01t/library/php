#!/bin/bash
# Stop on error
set -e
# Install mandatory programs and utilities
export DEBIAN_FRONTEND=noninteractive
apt-get --yes --quiet=2 update
apt-get --yes --quiet=2 install \
	apt-transport-https \
	ca-certificates \
	curl \
	dnsutils \
	git \
	libcurl4 \
	libpq5 \
	libxslt1.1 \
	libzip4 \
	libcurl4-openssl-dev \
	libfreetype6 \
	libfreetype6-dev \
	libgmp-dev \
	libicu-dev \
	libmagickwand-6.q16-6 \
	libmagickwand-dev \
	libonig-dev \
	libpq-dev \
	libjpeg-dev \
	libjpeg62-turbo \
	libjpeg62-turbo-dev \
	libpng-dev \
	libxslt1-dev \
	libzip-dev \
	procps \
	rsync \
	wget \
	unzip \
	zip
if [ -n "${1}" ] && [ "${1}" = '-apache' ]
then
	apt-get --yes --quiet=2 install apache2
	a2enmod deflate expires http2 proxy proxy_connect proxy_fcgi proxy_http proxy_http2 proxy_wstunnel rewrite sed ssl substitute
	cat <<EOF > /etc/apache2/sites-available/000-default.conf
<VirtualHost *:80>
	ServerName localhost
	DocumentRoot /var/www/html
	DirectoryIndex index.php index.html
	<Directory "/var/www/html">
		AllowOverride All
		Require all granted
	</Directory>
	<FilesMatch ".+\.ph(p[3457]?|t|tml)$">
		SetHandler "proxy:fcgi://localhost:9000"
	</FilesMatch>
</VirtualHost>
EOF
fi
# Add php extensions
if php -r 'exit(PHP_VERSION_ID < 70400 ? 0 : 1);'
then
	docker-php-ext-configure gd --with-freetype-dir=/usr --with-jpeg-dir=/usr --with-png-dir=/usr
else
    docker-php-ext-configure gd --with-freetype --with-jpeg
fi
docker-php-ext-install -j$(nproc) bcmath exif gd gmp intl mbstring mysqli opcache pdo pdo_mysql pdo_pgsql soap sockets xsl zip
pecl install imagick redis xdebug
docker-php-ext-enable imagick redis
pecl clear-cache
# Configure xdebug but disable it
cat <<EOF > /usr/local/etc/php/conf.d/xdebug.ini
[xdebug]
xdebug.remote_enable=1
xdebug.remote_connect_back=1
xdebug.remote_port=9001
xdebug.auto_trace=0
xdebug.trace_enable_trigger=1
xdebug.trace_output_dir=/tmp
xdebug.trace_output_name=trace.%H.%R.%u
xdebug.profiler_enable=0
xdebug.profiler_enable_trigger=1
xdebug.profiler_output_dir=/tmp
xdebug.profiler_output_name=cachegrind.%H.%R.%u
EOF
# Mcrypt
if php -r 'exit(PHP_VERSION_ID < 70200 ? 0 : 1);'
then
	apt-get --yes --quiet=2 install libmcrypt4 libmcrypt-dev
	docker-php-ext-install -j$(nproc) mcrypt
	apt-get remove -y --auto-remove libmcrypt-dev
fi
# Purge unnecessary files
apt-get remove -y --auto-remove libcurl4-openssl-dev libfreetype6-dev libgmp-dev libicu-dev libjpeg-dev libjpeg62-turbo-dev libmagickwand-dev libonig-dev libpng-dev libpq-dev libxslt1-dev libzip-dev
apt-get clean
rm -rf /var/lib/apt/lists/*
# Add composer to path
cat <<EOF > /etc/profile.d/composer.sh
#!/bin/sh
export GLOBAL_COMPOSER_HOME="/usr/local/share/composer"
export PATH="\${GLOBAL_COMPOSER_HOME}/vendor/bin:\${PATH}"
export COMPOSER_ALLOW_SUPERUSER=1
export COMPOSER_NO_INTERACTION=1
EOF
source /etc/profile.d/composer.sh
mkdir -p /usr/local/share/composer
ln -s /usr/local/share/composer /root/.composer
# Fecth composer and install at /usr/local/bin/composer
mkdir -p /usr/local/bin
cd /usr/local/bin
curl -sS https://getcomposer.org/installer | php
chmod +x /usr/local/bin/composer.phar
mv /usr/local/bin/composer.phar /usr/local/bin/composer
# Change default shell for web user
usermod -s /bin/bash www-data
# Entrypoint with correct rights
cat <<EOF > /root/entrypoint.sh
#!/bin/bash
if [ -n "\${HOST_USER_ID}" ]
then
	usermod -o -u "\${HOST_USER_ID}" www-data
	groupmod -o -g "\${HOST_GROUP_ID}" www-data
fi
if [ -n "\${DEBUG}" ] && [ "\${DEBUG}" == "1" ]
then
	docker-php-ext-enable xdebug
fi
if [ -f /etc/apache2/sites-available/000-default.conf ] && [ -n "\${DOCUMENT_ROOT_SUBDIR}" ]
then
	sed -i -E "s#DocumentRoot /var/www/html.*#DocumentRoot /var/www/html/\${DOCUMENT_ROOT_SUBDIR}#g" /etc/apache2/sites-available/000-default.conf
fi
if [ -n "\${DEBUG}" ] && [ "\${DEBUG}" == "1" ] && [ -n "\$1" ]
then
	. /etc/profile
	exec "\$@"
else
	[ -f /var/www/html/bin/update.sh ] && su www-data -m -c "cd /var/www/html && bin/update.sh" || true
	if [ -f /etc/apache2/sites-available/000-default.conf ]
	then
		apache2ctl start
	fi
	php-fpm
fi
EOF
chmod +x /root/entrypoint.sh
