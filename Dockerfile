ARG VERSION=latest

FROM php:${VERSION}

ARG FLAVOR=

COPY install.sh /root/install.sh

RUN /bin/bash /root/install.sh ${FLAVOR}

ENTRYPOINT [ "/root/entrypoint.sh" ]
